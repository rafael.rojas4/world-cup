import IRepository from '../Domain/Interfaces/IRepository';
import teams from '../fakeData/fakeTeams.json'


export default class JsonRepository implements IRepository {

	public connect() { }

	public disconnect() { }
	
    public getResultsByGroup() {
        return teams;
    }
}