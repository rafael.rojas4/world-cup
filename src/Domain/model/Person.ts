export default class Person {
    private ci: number;
    private name: string;
    private nationality: string;
    private dateOfBirth: Date;

	constructor(ci: number, name: string, nationality: string, dateOfBirth: Date) {
		this.ci = ci;
        this.name = name;
        this.nationality = nationality;
        this.dateOfBirth = dateOfBirth;
	}
    public getCi(){
        return this.ci;
    }

}