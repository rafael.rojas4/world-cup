import Player from "./Player";

export default class Goal {
	private player: Player;
	private time: Date;

	constructor(player: Player, time: Date) {
		this.player = player;
		this.time = time;
	}

}