import Person from "./Person";

export default class Player extends Person{
    private shirtNumber: number;

	constructor(ci: number,name: string,nationality: string,dateOfBirth: Date,shirtNumber: number) {
        super(ci,name,nationality,dateOfBirth);
		this.shirtNumber = shirtNumber;
	}

}