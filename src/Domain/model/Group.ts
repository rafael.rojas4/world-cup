import Team from "./Team";

export default class Group {
    private name: string;
	private teams: Team[];

	constructor(teams: Team[], name: string) {
		this.teams = teams;
		this.name = name;
	}

	public get groupName() {
		return this.name;
	}

    public get groupTeams() {
		return this.teams;
	}

}