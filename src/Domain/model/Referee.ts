import Person from "./Person";

export default class Referee extends Person {
    private position: string;

    constructor(ci: number, name: string, nationality: string, dateOfBirth: Date, position: string) {
        super(ci, name, nationality, dateOfBirth);
        this.position = position;
    }

}