import Goal from "./Goal";
import Player from "./Player";
import Referee from "./Referee";
import Stadium from "./Stadium";
import Team from "./Team";

export default class Game {
	private teams: Team[];
	private date: Date;
	private referees: Referee[];
    private hourStart: number;
    private hourFinish: number;
    private stadium: Stadium;
    private goals: Goal[];

	constructor(teams: Team[], date: Date, referees: Referee[], hourStart: number, hourFinish: number,stadium: Stadium, goals: Goal[]) {
		this.teams = teams;
		this.date = date;
		this.referees = referees;
        this.hourStart = hourStart;
        this.hourFinish = hourFinish;
        this.stadium = stadium;
        this.goals = goals;
	}

    public createGoal(player:Player, time:Date){
        this.goals.push(new Goal(player,time));
    }

    /*public goalToTeam(player: Player){
        this.teams.forEach(team => {
            team.getPlayers().forEach(teamPlayer => {if(teamPlayer.getCi() === player.getCi()){
                team.addGoalOnFavor();
            }});
        });
    }*/

}