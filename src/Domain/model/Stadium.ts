export default class Stadium {
	private name: string;
	private ubication: string;
	private capacity: number;

	constructor(name: string, ubication: string, capacity: number) {
		this.name = name;
		this.ubication = ubication;
		this.capacity = capacity;
	}
}