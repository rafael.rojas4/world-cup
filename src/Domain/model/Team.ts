import Player from "./Player";

export default class Team {
    private name: string;
    private gamesPlayed: number;
    private gamesWon: number;
    private gamesLose: number;
    private gamestie: number;
    private goalsOnFavor: number;
    private goalsAgainst: number;
    private points: number;

    constructor(name: string) {
        this.name = name;
        this.gamesPlayed = 0;
        this.gamesWon = 0;
        this.gamesLose = 0;
        this.gamestie = 0;
        this.goalsOnFavor = 0;
        this.goalsAgainst = 0;
        this.points = 0;
    }

    public getPoints(){
        return this.points;
    }

    public addGoalOnFavor() {
        this.goalsOnFavor = this.goalsOnFavor + 1;
    }

    public addGoalAgainst() {
        this.goalsAgainst = this.goalsAgainst + 1;
    }
    public addGamesWon() {
        this.gamesWon = this.gamesWon + 1;
    }
    public addGamesLose() {
        this.gamesLose = this.gamesLose + 1;
    }
    public addGamesTie() {
        this.gamestie = this.gamestie + 1;
    }

}