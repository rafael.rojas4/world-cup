import Stadium from "./Stadium";

export default class Championship {
	private name: string;
	private stadiums: Stadium[];
	private countryName: string;
	
	constructor(name: string, countryName: string) {
		this.name = name;
		this.stadiums = [];
		this.countryName = countryName;
	}
}