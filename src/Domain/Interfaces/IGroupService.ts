import Group from "../model/Group";
import Team from "../model/Team";

export interface IGroupService {
    getResultsByGroup(): Group[];
    getWinnersByGroup(teams: Team[]): Team[];
    sortGroupByPoints(teams: Team[]): Team[];

}