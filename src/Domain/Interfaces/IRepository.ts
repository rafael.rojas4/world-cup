export default interface IRepository{
	connect(): void;
	disconnect(): void;
    getResultsByGroup(): any;
}