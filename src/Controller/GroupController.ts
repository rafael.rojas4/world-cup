import Group from "../Domain/model/Group";
import IRepository from "../Domain/Interfaces/IRepository";
import GroupService from "../Service/GroupService";
import { IGroupService } from "../Domain/Interfaces/IGroupService";

export default class GroupController {
	private groupService: IGroupService;

	constructor(groupService: IGroupService) {
		this.groupService = groupService;
	}

	getResultsByGroup(): void {
		const groupsResult = this.groupService.getResultsByGroup();
		groupsResult.forEach((group: Group) => {
			console.log(`Group: ${group.groupName}`);
			console.table(this.groupService.sortGroupByPoints(group.groupTeams));
		});
	}
}