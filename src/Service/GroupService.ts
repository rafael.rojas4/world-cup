import Group from "../Domain/model/Group";
import IRepository from "../Domain/Interfaces/IRepository";
import Team from "../Domain/model/Team";


export default class GroupService {
	private repository: IRepository;

	constructor(repository: IRepository) {
		this.repository = repository;
	}

	public getResultsByGroup(): Group[] {
		const groups: Group[] = [];
		const result = this.repository.getResultsByGroup(); 
		result.forEach((element: any) => {
			let listTeams: Team[] = [];
			element.teams.forEach((team: any) => {
				listTeams.push(new Team(
					team.name, 
					team.gamesPlayed, 
					team.gamesWon, 
					team.gamesLose, 
					team.gamesTie, 
					team.goalsOnFavor, 
					team.goalsAgainst, 
					team.points
				));
			});
			groups.push(new Group([...listTeams], element.name));
			listTeams = [];
		});
		return groups;
	}

	public sortGroupByPoints(teams: Team[]){
        teams.sort((a: Team, b: Team) => b.getPoints() - a.getPoints());
        return teams;
    }

    public getWinnersByGroup(teams: Team[]){
        let teamWinners: Team[] = [];
        teams.sort((a, b) => a.getPoints() - b.getPoints());
        teamWinners[0]= teams[0];
        teamWinners[1]= teams[1];
        return teamWinners;
    }
}