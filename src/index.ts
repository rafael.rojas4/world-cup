import GroupController from "./Controller/GroupController";
import JsonRepository from "./Repository/JsonRepository";
import GroupService from "./Service/GroupService";

const repository = new JsonRepository();
const groupService = new GroupService(repository);
const groupController = new GroupController(groupService);

groupController.getResultsByGroup();


